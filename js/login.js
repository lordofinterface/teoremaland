var loginForm = document.getElementById("login");
var loginLogin = document.getElementById("login_login");
var loginPass = document.getElementById("login_password");
var regForm = document.getElementById("reg");
var regEmail = document.getElementById("reg_email");
var regPass1 = document.getElementById("reg_password1");
var regPass2 = document.getElementById("reg_password2");
var loginInfo = document.getElementsByClassName("block__info--login")[0];
var regCommon = document.getElementsByClassName("block__info--common")[0];
var regShort = document.getElementsByClassName("block__info--short")[0];

var LOGIN_URL = "http://oculars.net/rest-auth/login/";
var REG_URL = "http://oculars.net/rest-auth/registration/";

var onLoginSubmit = function(e) {
  e.preventDefault();
  var request = {
    username: loginLogin.value,
    password: loginPass.value
  };
  axios.post(LOGIN_URL, request).then(
    function(res) {
      localStorage.setItem("TEOREMA_TOKEN", res.data.key);
    },
    function(res) {
      loginInfo.classList.add("visible");
    }
  );
};

var onRegSubmit = function(e) {
  regCommon.classList.remove("visible");
  regShort.classList.remove("visible");
  e.preventDefault();
  if (regPass1.value !== regPass2.value) {
    regCommon.classList.add("visible");
    return;
  }
  var request = {
    email: regEmail.value,
    password1: regPass1.value
  };
  axios.post(REG_URL, request).then(
    function(res) {
      localStorage.setItem("TEOREMA_TOKEN", res.data.key);
    },
    function(res) {
      if ((res.response.data.password1[0] = "easy")) {
        regShort.classList.add("visible");
      }
    }
  );
};

loginForm.addEventListener("submit", onLoginSubmit);
regForm.addEventListener("submit", onRegSubmit);
