const gulp = require('gulp');
const scss = require('gulp-sass');
const browserSync = require('browser-sync');
const imagemin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');
const uglify = require('gulp-uglify');
const cssClean = require('gulp-clean-css');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');
const webpackConfig = require('./webpack.config.js');

var paths = {
  fonts: ['fonts/*'],
  js: ['js/*.js'],
  app: ['js/app/*.js'],
  html: ['*.html'],
  scss: ['scss/*'],
  img: ['img/**/*.{png,jpg,jpeg,gif,svg}']
};

gulp.task('scss', function() {
  return gulp
    .src(paths.scss)
    .pipe(scss().on('error', scss.logError))
    .pipe(
      autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
      })
    )
    .pipe(cssClean())
    .pipe(gulp.dest('build/css'))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('js', function() {
  return gulp
    .src(paths.js)
    .pipe(uglify())
    .pipe(gulp.dest('build/js'))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('app', function() {
  return gulp
    .src(paths.app)
    .pipe(webpackStream(webpackConfig), webpack)
    .pipe(gulp.dest('build/js'))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('fonts', function() {
  return gulp
    .src(paths.fonts)
    .pipe(gulp.dest('build/fonts'))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('html', function() {
  return gulp
    .src(paths.html)
    .pipe(gulp.dest('build'))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('img', function() {
  return gulp
    .src(paths.img)
    .pipe(imagemin())
    .pipe(gulp.dest('build/img'));
});

gulp.task('browserSync', function() {
  browserSync({
    server: {
      baseDir: './build/'
    },
    port: 8000,
    notify: false
  });
});

gulp.task('watch', function() {
  gulp.watch(paths.scss, ['scss']);
  gulp.watch(paths.js, ['js']);
  gulp.watch(paths.app, ['app']);
  gulp.watch(paths.fonts, ['fonts']);
  gulp.watch(paths.html, ['html']);
  gulp.watch(paths.img, ['img']);
});

gulp.task('default', [
  'scss',
  'js',
  'app',
  'html',
  'img',
  'fonts',
  'watch',
  'browserSync'
]);
gulp.task('build', ['scss', 'js', 'app', 'html', 'img', 'fonts']);
gulp.task('default', ['scss', 'js', 'app', 'html', 'img', 'fonts', 'watch', 'browserSync']);
