const path = require('path');

const config = {
  output: {
    path: path.resolve('build/js'),
    filename: '[name].js'
  },
  module: {
    rules: [
      { test: /\.js$/, use: 'babel-loader' }
    ]
  }
};

module.exports = config;